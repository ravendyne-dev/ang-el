## Add Electron 11.x to your Angular 9.x UI


This is an Angular schematic that enables you to `ng add` all the bits and pieces to an Angular 9 project in order to run it as a UI part of an Electron 11.x app.

This schematic (for now) works only with:
- Angular 9.x
- Node 12.x
- Electron 11.x

For a detailed walk-through on how, where and why when it comes to the Electron app source, scripts and other stuff used in this schematic, see article at [https://ravendyne.com/angular/angular-electron-app]().


## How to use

Create Angular-and-Electron app from scratch:

```bash
$ ng new you-angular-app-name --no-routing
$ cd you-angular-app-name
$ npm install --save-dev @ravendyne/ang-el
$ ng add @ravendyne/ang-el

$ npm run build
$ npm run electron
```

If you have Angular workspace with multiple projects, running `ng add` as is above will select the first project in the workspace by default. If you want to add Electron to a specific project within your workspace, run instead:
```bash
$ ng add @ravendyne/ang-el --project=your-project-name
```

To package you application for the OS you are running this command on, run:
```bash
$ npm run build:package
```

Once packaging is done, your binary application with all the supporting files will be in the `build/you-angular-app-name-[whatever-the-OS]` folder, i.e. on 64-bit Linux it will be in `build/you-angular-app-name-linux-x64`.

Running `npm run build:package` on Linux will produce Linux binary, running it on Windows will produce Windows exe, and on MacOs will produce MacOS app.


## Options

Schematic will add a simple "Bridge API" (see [here](https://ravendyne.com/angular/angular-electron-app#communication-between-angular-ui-and-electron-backend) for more details) and it will set up [`electron-packager`](https://electron.github.io/electron-packager/main/) as a tool to build a binary distribution package of your app.

If you don't want the "Bridge API" added to your project, use `--no-bridge-api` CLI option. If you don't want `electron-packager` added and set up, use `--no-packager` CLI option, i.e.:
```bash
$ ng add @ravendyne/ang-el --no-bridge-api --no-packager
```

## Add application icons on Linux, Windows or MacOS

For details on how to set an icon for your packaged application that will be recognized and used by host OS, see details at [https://ravendyne.com/angular/angular-electron-app#app-icon-in-macos-and-windows]().
