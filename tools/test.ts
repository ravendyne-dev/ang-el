import { compile, compileFromFile } from 'json-schema-to-typescript'
import { writeFileSync } from 'fs';


// "build:tool": "ts-node -P tools/tsconfig.json tools/test.ts",
(async () => {

    console.log('RUN');
    // compile from file
    const ts = await compileFromFile('./src/ang-el/schema.json')
    // .then(ts => writeFileSync('schema.d.ts', ts))
    writeFileSync('schema.d.ts', ts);
    
    console.log('DONE');
    
    // // or, compile a JS object
    // let mySchema = {
    //   properties: [...]
    // }
    // compile(mySchema, 'MySchema')
    //   .then(ts => ...)
    
})();
