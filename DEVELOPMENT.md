## Install prerequisites for development

```bash
$ npm -g list --depth=0
/path-to-nodejs-v12/lib
├── @angular/cli@9.1.12
└── npm@6.14.8
```

Install schematics cli for Angular v9:

```bash
$ npm i -g @angular-devkit/schematics-cli@v9-lts
$ npm -g list --depth=0
/path-to-nodejs-v12/lib
├── @angular-devkit/schematics-cli@0.901.15
├── @angular/cli@9.1.12
└── npm@6.14.8
```

## Testing/debugging

Build this schematic:
```bash
$ npm run build
```

Create a test Angular project:

```bash
$ cd somewhere-else-but-this-schematic-project-folder
$ ng new my-app --no-routing --style=scss # optionally use --skip-install and do it yourself later
$ cd my-app
```

Test run this schematic:
```bash
$ schematics <relative-or-absolute-path-to-schematics-project-folder>:ng-add
```
This runs schematic in debug mode where it only outputs list of actions but does not commit them to file system.

To run the schematic **and** commit changes to file system, do:

```bash
$ schematics <relative-or-absolute-path-to-schematics-project-folder>:ng-add --no-dry-run
```


You can also use `npm pack` inside the schematics project, then `npm install /path/to/artifact.tar.gz` in the Angular project. This mimics `npm install`.

After that you can simply do:
```bash
$ ng add ang-el
```
inside the Angular project folder.
