import { chain, Rule, schematic, SchematicContext, Tree, } from '@angular-devkit/schematics';

export default function (options: any): Rule {
  return (_host: Tree, _context: SchematicContext) => {
    return chain([
        schematic('ang-el', options)
    ])
  };
}

