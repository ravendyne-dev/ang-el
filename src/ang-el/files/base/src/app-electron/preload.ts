// All of the Node.js APIs are available in the preload process.


// It has the same sandbox as a Chrome extension.
window.addEventListener("DOMContentLoaded", () => {
  const replaceText = (selector: string, text: string) => {
    const element = document.getElementById(selector);
    if (element) {
      element.innerText = text;
    }
  };

  for (const type of ["chrome", "node", "electron"]) {
    replaceText(`${type}-version`, process.versions[type as keyof NodeJS.ProcessVersions]);
  }
});


// setup some stuff in renderer process that requires 'electron' API
// but we can't call it because we use process isolation and we disabled Node integration

// For example, zoom in the UI by 20% by default:
import { webFrame } from 'electron';
webFrame.setZoomFactor(1.2);

