
export interface APIBridgeRendererItnf {
  fetch: ( method: string, ...args: any[] ) => Promise<any>;
}
