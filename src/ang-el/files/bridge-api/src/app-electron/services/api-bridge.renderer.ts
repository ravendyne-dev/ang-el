import { contextBridge } from 'electron';
import { ipcRenderer } from 'electron';

import { APIBridgeRendererItnf } from './api-bridge.renderer.interface';


export class APIBridgeRenderer implements APIBridgeRendererItnf {

  fetch( method: string, ...args: any[] ) : Promise<any> {
    const apiChannel = `api-bridge/${method}`;
    return ipcRenderer.invoke( apiChannel, ...args );
  }
}



export const setupRemoteAPIBridge = () => {

  const apiBridge = new APIBridgeRenderer();

  contextBridge.exposeInMainWorld( 'apiBridge', {
    // we can only do this because we are not referencing
    // any properties of APIBridgeRenderer in the fetch() method
    fetch: apiBridge.fetch,
  });

}
