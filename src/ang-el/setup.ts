import {
  getWorkspace,
  // buildDefaultPath
} from '@schematics/angular/utility/workspace';
// import { parseName } from '@schematics/angular/utility/parse-name';
// import { ProjectDefinition } from '@angular-devkit/core/src/workspace';
import { SchematicsException, Tree } from '@angular-devkit/schematics';
import { join, normalize } from 'path';

export const setupOptions = async (tree: Tree, options: any): Promise<Tree> => {
  // console.log('setupOptions OPTIONS pre',options)

  // Angular workspace is described by angular.json file
  const workspace = await getWorkspace(tree);

  // console.log('PROJECTS', workspace.projects.keys())

  if( ! options.project ) {
      // get the first project's name from the workspace
      // for( const [n, ] of workspace.projects ) { options.project = n; break; }
      options.project = workspace.projects.keys().next().value;
  }
  const project = workspace.projects.get(options.project);// as ProjectDefinition;
  if( ! project ) {
      throw new SchematicsException( `Invalid project name: ${options.project}` );
    }

  options.path = join( normalize( project.root || '' ) );

  // console.log('setupOptions OPTIONS post',options)

  return tree;
}
