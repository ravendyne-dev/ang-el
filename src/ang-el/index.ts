import { strings } from '@angular-devkit/core';
import { apply, chain, filter, MergeStrategy, mergeWith, move, noop, Rule, SchematicContext, SchematicsException, template, Tree, url } from '@angular-devkit/schematics';
import { NodePackageInstallTask } from '@angular-devkit/schematics/tasks';
import { latestVersions } from '@schematics/angular/utility/latest-versions';
import { addPackageJsonDependency, NodeDependency, NodeDependencyType } from '@schematics/angular/utility/dependencies';

import { join, normalize } from 'path';
import { satisfies as semverSatisfies, coerce as semverCoerce, SemVer } from 'semver';

import { setupOptions } from './setup';
import { SimpleSchematicOptions } from './schema';

// added in v10, we are at v9 so we'll borrow the class source file
// import { JSONFile } from '@schematics/angular/utility/json-file';
import { JSONFile } from './lib/json-file';
import { getWorkspace, updateWorkspace } from '@schematics/angular/utility/workspace';

import { BrowserBuilderOptions } from '@schematics/angular/utility/workspace-models';
import { targetBuildNotFoundError } from '@schematics/angular/utility/project-targets';


export const dependenciesBase: { [key: string]: string } = {
};

export const devDependenciesBase: { [key: string]: string } = {
  "@angular-builders/custom-webpack": "^9.2.0",
  "@types/node": "^12.20.15",
  "electron": "^11.4.10",
  "electron-reload": "^1.5.0",
  "rimraf": "^3.0.2",
  "watch": "^1.0.2",
};

const packagerScripts: {[key:string]:string} = {
  "build:package": "ts-node -P tools/tsconfig.tools.json tools/build-package.ts",
}

export const devDependenciesPackager: { [key: string]: string } = {
  "electron-packager": "^15.2.0",
};



export const angelSchematic = (options: SimpleSchematicOptions): Rule => {

  // console.log(latestVersions);
  const isAngularV9 = semverSatisfies( semverCoerce( latestVersions.Angular ) as SemVer, '9.x' );
  if( ! isAngularV9 ) {
    throw new SchematicsException( `Angular v9 required for this schematic to work. The project's Angular dependency is "${latestVersions.Angular}"` );
  }

  // console.log('OPTIONS',options)

  return chain([
    setup(options),

    // base
    addFiles('./files/base', options),
    addDependencies(dependenciesBase, devDependenciesBase),
    addBasePackageScripts(options),
    addAngularBuilders(options),

    // bridge api
    !!options['bridge-api'] ? addBridgeAPISchematic(options) : noop(),

    // packager
    options.packager ? addPackagerSchematic(options) : noop(),
  ]);
}

const addFiles = (filesUrl:string, options: SimpleSchematicOptions) => {
  return (_tree: Tree, _context: SchematicContext) => {

    const movePath = normalize(options.path + '');
    // console.log('MOVE PATH', movePath)

    const templateSource = apply( url(filesUrl), [
        options.spec ? noop() : filter(path => !path.endsWith('.spec.ts')),
        template({
            ...strings,
            ...options,
        }),
        move(movePath),
    ]);

    // return mergeWith( templateSource, MergeStrategy.Default );
    return mergeWith( templateSource, MergeStrategy.Overwrite );
  }
}

const setup = (options: SimpleSchematicOptions) => {
  return async (tree: Tree, _context: SchematicContext) => {

    await setupOptions(tree, options);
  }
}


////////////////////////////////////////////////////////////////
//
// BASE STUFF
//
////////////////////////////////////////////////////////////////

const addDependencies = ( dependencies: { [key: string]: string }, devDependencies: { [key: string]: string }): Rule => {
  return ( tree: Tree, _context: SchematicContext ) => {

    for( let pkg in dependencies ) {
      const nodeDependency: NodeDependency = {
        type: NodeDependencyType.Default,
        name: pkg,
        version: dependencies[pkg],
        overwrite: true
      };
      addPackageJsonDependency(tree, nodeDependency);
    }

    for( let pkg in devDependencies ) {
      const nodeDependency: NodeDependency = {
        type: NodeDependencyType.Dev,
        name: pkg,
        version: devDependencies[pkg],
        overwrite: true
      };
      addPackageJsonDependency(tree, nodeDependency);
    }

    // do npm install after all changes are in place
    _context.addTask(new NodePackageInstallTask());
  };
}

const addBasePackageScripts = ( options: SimpleSchematicOptions ): Rule => {
  return ( tree: Tree, _context: SchematicContext ) => {

    const pkgJson = new JSONFile(tree, 'package.json');

    pkgJson.modify(['main'], 'dist/main.js');

    // console.log('OPTIONS',options)
    const scripts: {[key:string]:string} = {
      "clean": "rimraf dist build",
      "build": "npm run clean && npm run build:angular && npm run build:electron && npm run build:electronp",
      "build:angular": "ng build --base-href ./",
      "build:electron": `ng run ${options.project}:electron`,
      "build:electronp": `ng run ${options.project}:electron-preload`,
      "watch:angular": "ng build --base-href ./ --watch",
      "watch:electron": "watch 'npm run build:electron && npm run build:electronp' ./src/app-electron",
      "electron": "electron .",
    }

    for( const name in scripts ) {
      const script = scripts[name];
      pkgJson.modify(['scripts',name], script, false);
    }
  };
}

const addAngularBuilders = ( options: SimpleSchematicOptions ): Rule => {
  return async ( tree: Tree, _context: SchematicContext ) => {

    const workspace = await getWorkspace(tree);

    const project = workspace.projects.get(options.project as string);
    if( ! project ) {
      throw new SchematicsException(`Project name "${options.project}" does not exist in this workspace.`);
    }

    project.targets.add({
      name: 'electron',
      builder: "@angular-devkit/build-angular:server",
      options: {
        outputPath: "dist",
        main: "src/app-electron/main.ts",
        tsConfig: "tsconfig.el.json",
        deleteOutputPath: false,
        externalDependencies: [
          "electron",
          "electron-reload"
        ]
      },
      configurations: {
        production: {
          outputHashing: "media",
          fileReplacements: [
            {
              replace: "src/environments/environment.ts",
              with: "src/environments/environment.prod.ts"
            }
          ],
          sourceMap: false,
          optimization: true
        }
      }
    });

    project.targets.add({
      name: 'electron-preload',
      builder: "@angular-builders/custom-webpack:server",
      options: {
        customWebpackConfig: {
          path: "./webpack.config.elp.js",
          mergeStrategies: {
            "module.rules": "prepend"
          }
        },
        outputPath: "dist",
        main: "src/app-electron/preload.ts",
        tsConfig: "tsconfig.elp.json",
        deleteOutputPath: false,
        externalDependencies: [
          "electron",
          "electron-reload"
        ]
      },
      configurations: {
        production: {
          outputHashing: "media",
          fileReplacements: [
            {
              replace: "src/environments/environment.ts",
              with: "src/environments/environment.prod.ts"
            }
          ],
          sourceMap: false,
          optimization: true
        }
      }
    });

    const buildTarget = project.targets.get('build');
    if( ! buildTarget ) {
      throw targetBuildNotFoundError();
    }
    const clientBuildOptions = ((buildTarget.options || {}) as unknown) as BrowserBuilderOptions;
    clientBuildOptions.outputPath = 'dist/ui';
    // TODO add icons
    // clientBuildOptions.assets?.push('src/favicon.png');

    return updateWorkspace( workspace );
  };
}


////////////////////////////////////////////////////////////////
//
// BRIDGE API
//
////////////////////////////////////////////////////////////////

const addBridgeAPISchematic = (options: SimpleSchematicOptions): Rule => {

  return chain([
    addFiles('./files/bridge-api', options),
    updatePreloadTs(options),
  ]);
}

const updatePreloadTs = (options: SimpleSchematicOptions): Rule => {
  const code =
`
import { setupRemoteAPIBridge } from './services/api-bridge.renderer';
setupRemoteAPIBridge();
`;

  return (tree: Tree, _context: SchematicContext) => {

    const preloadTsPath = join( options.path as string, 'src/app-electron/preload.ts' );

    const preloadFile = tree.get(preloadTsPath);
    if( ! preloadFile ) {
      throw new SchematicsException(`Could not find 'preload.ts'.`);
    }

    // TODO: use UpdateRecorder?
    const newCode = preloadFile.content.toString() + code;
    tree.overwrite(preloadTsPath, newCode);

    // const recorder = tree.beginUpdate(preloadTsPath);
    // tree.commitUpdate(recorder);

    return tree;
  }

}


////////////////////////////////////////////////////////////////
//
// PACKAGER STUFF
//
////////////////////////////////////////////////////////////////

const addPackagerSchematic = (options: SimpleSchematicOptions): Rule => {

  return chain([
    addFiles('./files/packager', options),
    addPackageScripts( packagerScripts ),
    addDependencies({}, devDependenciesPackager),
  ]);
}

const addPackageScripts = ( scripts: {[key:string]:string} ): Rule => {
  return ( tree: Tree, _context: SchematicContext ) => {

    const pkgJson = new JSONFile(tree, 'package.json');

    for( const name in scripts ) {
      const script = scripts[name];
      pkgJson.modify(['scripts',name], script, false);
    }
  };
}
