export const dependenciesBase: { [key: string]: string } = {
};

export const devDependenciesBase: { [key: string]: string } = {
  "@angular-builders/custom-webpack": "^9.2.0",
  "@types/node": "^12.20.15",
  "electron": "^11.4.10",
  // "electron-packager": "^15.2.0",
  "electron-reload": "^1.5.0",
  "rimraf": "^3.0.2",
  "watch": "^1.0.2",
};
