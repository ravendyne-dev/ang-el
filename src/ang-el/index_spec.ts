// import { Tree } from '@angular-devkit/schematics';
import { SchematicTestRunner, UnitTestTree } from '@angular-devkit/schematics/testing';
import * as path from 'path';

import { Schema as WorkspaceOptions } from '@schematics/angular/workspace/schema';
import { Schema as ApplicationOptions } from '@schematics/angular/application/schema';
import { SimpleSchematicOptions } from './schema';


const collectionPath = path.join(__dirname, '../collection.json');


describe('ang-el', () => {

  const runner = new SchematicTestRunner( 'schematics', collectionPath );

  const workspaceOptions: WorkspaceOptions = { 
    name: 'workspace',
    newProjectRoot: 'projects',
    version: '0.5.0',
  };

  const appOptions: ApplicationOptions = { 
    name: 'schematest',
    projectRoot: '',
  };

  const schemaOptionsAll: SimpleSchematicOptions = {
    // project: '',
    'bridge-api': true,
    packager: true,
  };

  const expectedNewFiles_All = [

    '/tsconfig.el.json',
    '/tsconfig.elp.json',
    '/webpack.config.elp.js',

    '/src/app-electron/main.ts',
    '/src/app-electron/preload.ts',

    '/src/app/services/api-bridge.ts',
    '/src/app-electron/services/api-bridge.main.ts',
    '/src/app-electron/services/api-bridge.renderer.interface.ts',
    '/src/app-electron/services/api-bridge.renderer.ts',

    '/tools/build-package.ts',
    '/tools/tsconfig.tools.json'
  ]

  const schemaOptionsBaseOnly: SimpleSchematicOptions = {
    // project: '',
    'bridge-api': false,
    packager: false,
  };

  const expectedNewFiles_BaseOnly = [

    '/tsconfig.el.json',
    '/tsconfig.elp.json',
    '/webpack.config.elp.js',

    '/src/app-electron/main.ts',
    '/src/app-electron/preload.ts',
  ]

  const schemaOptionsNoBridgeAPI: SimpleSchematicOptions = {
    // project: '',
    'bridge-api': false,
    packager: true,
  };

  const expectedNewFiles_NoBridgeAPI = [

    '/tsconfig.el.json',
    '/tsconfig.elp.json',
    '/webpack.config.elp.js',

    '/src/app-electron/main.ts',
    '/src/app-electron/preload.ts',

    '/tools/build-package.ts',
    '/tools/tsconfig.tools.json'
  ]

  const schemaOptionsNoPackager: SimpleSchematicOptions = {
    // project: '',
    'bridge-api': true,
    packager: false,
  };

  const expectedNewFiles_NoPackager = [

    '/tsconfig.el.json',
    '/tsconfig.elp.json',
    '/webpack.config.elp.js',

    '/src/app-electron/main.ts',
    '/src/app-electron/preload.ts',

    '/src/app/services/api-bridge.ts',
    '/src/app-electron/services/api-bridge.main.ts',
    '/src/app-electron/services/api-bridge.renderer.interface.ts',
    '/src/app-electron/services/api-bridge.renderer.ts',
  ]


  let appTree: UnitTestTree;

  beforeEach(async () => { 
    appTree = await runner.runExternalSchematicAsync('@schematics/angular', 'workspace', workspaceOptions).toPromise();
    appTree = await runner.runExternalSchematicAsync('@schematics/angular', 'application', appOptions, appTree).toPromise();
  });

  it('adds base files', async () => {

    await runner.runSchematicAsync('ang-el', schemaOptionsAll, appTree/*Tree.empty()*/).toPromise().then( tree => {
      // console.log('FILES', tree.files)

      // const angularJson = tree.readContent('/angular.json'); 
      // console.log('ANGULAR.JSON',angularJson)

      // const packageJson = tree.readContent('/package.json'); 
      // console.log('PACKAGE.JSON',packageJson)

      // const preloadTs = tree.readContent('src/app-electron/preload.ts'); 
      // console.log('PRELOAD.TS',preloadTs)
      
      expect(tree.files).toEqual(jasmine.arrayContaining( expectedNewFiles_All ));

      // TODO: test if package.json contains scripts we added
      // tree.readContent('package.json')
    })
  });

  it('skips bridge api when option is cleared', async () => {

    await runner.runSchematicAsync('ang-el', schemaOptionsNoBridgeAPI, appTree/*Tree.empty()*/).toPromise().then( tree => {
      // console.log('FILES', tree.files)

      // const preloadTs = tree.readContent('src/app-electron/preload.ts'); 
      // console.log('PRELOAD.TS',preloadTs)
      
      expect(tree.files).toEqual(jasmine.arrayContaining( expectedNewFiles_NoBridgeAPI ));

      // TODO: test if package.json contains scripts we added
      // tree.readContent('package.json')
    })
  });

  it('skips packager when option is cleared', async () => {

    await runner.runSchematicAsync('ang-el', schemaOptionsNoPackager, appTree/*Tree.empty()*/).toPromise().then( tree => {
      // console.log('FILES', tree.files)

      // const packageJson = tree.readContent('/package.json'); 
      // console.log('PACKAGE.JSON',packageJson)
      
      expect(tree.files).toEqual(jasmine.arrayContaining( expectedNewFiles_NoPackager ));

      // TODO: test if package.json contains scripts we added
      // tree.readContent('package.json')
    })
  });

  it('creates only base files when packager and bridge-api options are cleared', async () => {

    await runner.runSchematicAsync('ang-el', schemaOptionsBaseOnly, appTree/*Tree.empty()*/).toPromise().then( tree => {
      // console.log('FILES', tree.files)

      // const packageJson = tree.readContent('/package.json'); 
      // console.log('PACKAGE.JSON',packageJson)
      
      expect(tree.files).toEqual(jasmine.arrayContaining( expectedNewFiles_BaseOnly ));

      // TODO: test if package.json contains scripts we added
      // tree.readContent('package.json')

      // TODO: test if angular.json contains builders we added
    })
  });
});
